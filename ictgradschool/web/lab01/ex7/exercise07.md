Herbivore:

* Horse
* Cow
* Sheep
* Capybara
* Chicken

Carnivorous:

* Cat
* Dog
* Frog
* Hawk
* Polar Bear
* Lion
* Fox

Omnivorous:

* Rat
* Raccoon